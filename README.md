# Static one-page website from a markdown file

This project uses [Hugo static websites generator](https://gohugo.io/) to generate a static website from a markdown file.

## How to generate

- create the content file: `dataset_page/content/_index.md`
- to test locally, you can use `start_server.sh`
- when you're happy, use `generate.sh` to generate static website
    - the output will be placed in `dataset_page/public/` subdir
